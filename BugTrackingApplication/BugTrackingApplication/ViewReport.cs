﻿using BusinessLogicLayer;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingApplication
{
    public partial class ViewReport : Form
    {

        SqlConnection conn = new SqlConnection(ConnectionClass.ConnectionString);

        private int bugId;
        private string filepath; // image path

        BugClass bugClass = new BugClass();
        BusinessLogicClass blc = new BusinessLogicClass();

     

        public ViewReport(int bugId)
        {
            this.bugId = bugId;
            InitializeComponent();
            loadBug();


        }
        public ViewReport()
        {
            InitializeComponent();
            loadBug();
        }

        public void loadBug()
        {

            try
            {
                conn.Open();
                String selectQuery = "SELECT * FROM BugTable WHERE BugId = '" + bugId.ToString() + "'";

                SqlCommand command = new SqlCommand(selectQuery, conn);

                SqlDataAdapter da = new SqlDataAdapter(command);

                DataTable table = new DataTable();

                da.Fill(table);
                txtId.Text = table.Rows[0][0].ToString();
                txtprojectName.Text = table.Rows[0][1].ToString();
                txtreportedDate.Text = table.Rows[0][2].ToString();
                txtfixedDate.Text = table.Rows[0][3].ToString();
                txtBug.Text = table.Rows[0][4].ToString();
                richTextBoxBugDescription.Text = table.Rows[0][5].ToString();
                txtreportedBy.Text = table.Rows[0][6].ToString();
                txtfixedBy.Text = table.Rows[0][7].ToString();
                byte[] errorImage = (byte[])table.Rows[0][8];

                byte[] solvedImages = (byte[])table.Rows[0][9];
                textBoxBugSolution.Text = table.Rows[0][10].ToString();
                linkLabel1.Text = table.Rows[0][11].ToString();
                txtbugStatus.Text = table.Rows[0][12].ToString();

                MemoryStream ms_solvedImages = new MemoryStream(solvedImages);
                MemoryStream ms_errorImage = new MemoryStream(errorImage);
                pbErrorImage.Image = Image.FromStream(ms_errorImage);
                pbBugSolution.Image = Image.FromStream(ms_solvedImages);



                da.Dispose();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error: " + ex.Message);
            }
        }

        private void ViewReport_Load(object sender, EventArgs e)
        {
            //get syntax color on form viewBug_load


            // getting keywords/functions
            string keywords = @"\b(public|private|partial|static|namespace|class|using|void|foreach|in)\b";
            MatchCollection keywordMatches = Regex.Matches(richTextBoxBugDescription.Text, keywords);

            // getting types/classes from the text 
            string types = @"\b(Console)\b";
            MatchCollection typeMatches = Regex.Matches(richTextBoxBugDescription.Text, types);

            // getting comments (inline or multiline)
            string comments = @"(\/\/.+?$|\/\*.+?\*\/)";
            MatchCollection commentMatches = Regex.Matches(richTextBoxBugDescription.Text, comments, RegexOptions.Multiline);

            // getting strings
            string strings = "\".+?\"";
            MatchCollection stringMatches = Regex.Matches(richTextBoxBugDescription.Text, strings);

            // saving the original caret position + forecolor
            int originalIndex = richTextBoxBugDescription.SelectionStart;
            int originalLength = richTextBoxBugDescription.SelectionLength;
            Color originalColor = Color.Black;

            // MANDATORY - focuses a label before highlighting (avoids blinking)
            label4.Focus();

            // removes any previous highlighting (so modified words won't remain highlighted)
            richTextBoxBugDescription.SelectionStart = 0;
            richTextBoxBugDescription.SelectionLength = richTextBoxBugDescription.Text.Length;
            richTextBoxBugDescription.SelectionColor = originalColor;

            // scanning...
            foreach (Match m in keywordMatches)
            {
                richTextBoxBugDescription.SelectionStart = m.Index;
                richTextBoxBugDescription.SelectionLength = m.Length;
                richTextBoxBugDescription.SelectionColor = Color.Blue;
            }

            foreach (Match m in typeMatches)
            {
                richTextBoxBugDescription.SelectionStart = m.Index;
                richTextBoxBugDescription.SelectionLength = m.Length;
                richTextBoxBugDescription.SelectionColor = Color.DarkCyan;
            }

            foreach (Match m in commentMatches)
            {
                richTextBoxBugDescription.SelectionStart = m.Index;
                richTextBoxBugDescription.SelectionLength = m.Length;
                richTextBoxBugDescription.SelectionColor = Color.Green;
            }

            foreach (Match m in stringMatches)
            {
                richTextBoxBugDescription.SelectionStart = m.Index;
                richTextBoxBugDescription.SelectionLength = m.Length;
                richTextBoxBugDescription.SelectionColor = Color.Brown;
            }

            // restoring the original colors, for further writing
            richTextBoxBugDescription.SelectionStart = originalIndex;
            richTextBoxBugDescription.SelectionLength = originalLength;
            richTextBoxBugDescription.SelectionColor = originalColor;

            // giving back the focus
            richTextBoxBugDescription.Focus();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(linkLabel1.Text);
        }
    }
}
