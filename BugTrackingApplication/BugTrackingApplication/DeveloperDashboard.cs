﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingApplication
{
    public partial class DeveloperDashboard : Form
    {
        private string reporter;
        public DeveloperDashboard()
        {
            InitializeComponent();
        }
        public DeveloperDashboard(string reporter)
        {
            this.reporter = reporter;
            InitializeComponent();
        }
       

        private void viewBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportBug rb = new ReportBug(reporter);
            rb.Show();
        }

        private void viewBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BugList list = new BugList(reporter);
            list.Show();
        }

        private void bugToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void viewProjectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProjectForm frm = new ProjectForm();
            frm.Show();
        }

        private void searchBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int role = 1;
            SearchBug bug = new SearchBug(role);
            bug.Show();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword cp = new BugTrackingApplication.ChangePassword(reporter);
            cp.Show();
        }
    }
}
