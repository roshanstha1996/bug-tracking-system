﻿namespace BugTrackingApplication
{
    partial class ViewReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbErrorImage = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pbBugSolution = new System.Windows.Forms.PictureBox();
            this.richTextBoxBugDescription = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBugSolution = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtfixedBy = new System.Windows.Forms.TextBox();
            this.txtreportedBy = new System.Windows.Forms.TextBox();
            this.txtfixedDate = new System.Windows.Forms.TextBox();
            this.txtreportedDate = new System.Windows.Forms.TextBox();
            this.txtprojectName = new System.Windows.Forms.TextBox();
            this.txtbugStatus = new System.Windows.Forms.TextBox();
            this.txtBug = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBugSolution)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbErrorImage
            // 
            this.pbErrorImage.Location = new System.Drawing.Point(501, 93);
            this.pbErrorImage.Name = "pbErrorImage";
            this.pbErrorImage.Size = new System.Drawing.Size(469, 192);
            this.pbErrorImage.TabIndex = 123;
            this.pbErrorImage.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(434, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 122;
            this.label9.Text = "Error Image";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 806);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 121;
            this.label8.Text = "Bug Solution";
            // 
            // pbBugSolution
            // 
            this.pbBugSolution.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbBugSolution.Location = new System.Drawing.Point(153, 806);
            this.pbBugSolution.Name = "pbBugSolution";
            this.pbBugSolution.Size = new System.Drawing.Size(364, 167);
            this.pbBugSolution.TabIndex = 120;
            this.pbBugSolution.TabStop = false;
            // 
            // richTextBoxBugDescription
            // 
            this.richTextBoxBugDescription.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBoxBugDescription.Location = new System.Drawing.Point(140, 12);
            this.richTextBoxBugDescription.Name = "richTextBoxBugDescription";
            this.richTextBoxBugDescription.ReadOnly = true;
            this.richTextBoxBugDescription.Size = new System.Drawing.Size(817, 273);
            this.richTextBoxBugDescription.TabIndex = 10;
            this.richTextBoxBugDescription.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Bug Description";
            // 
            // textBoxBugSolution
            // 
            this.textBoxBugSolution.Location = new System.Drawing.Point(153, 624);
            this.textBoxBugSolution.Multiline = true;
            this.textBoxBugSolution.Name = "textBoxBugSolution";
            this.textBoxBugSolution.ReadOnly = true;
            this.textBoxBugSolution.Size = new System.Drawing.Size(683, 163);
            this.textBoxBugSolution.TabIndex = 113;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 589);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 13);
            this.label10.TabIndex = 111;
            this.label10.Text = "Version Control Link";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 632);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 112;
            this.label11.Text = "Bug Solution";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.richTextBoxBugDescription);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(11, 291);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(959, 288);
            this.panel1.TabIndex = 110;
            // 
            // txtfixedBy
            // 
            this.txtfixedBy.Location = new System.Drawing.Point(610, 49);
            this.txtfixedBy.Name = "txtfixedBy";
            this.txtfixedBy.ReadOnly = true;
            this.txtfixedBy.Size = new System.Drawing.Size(169, 20);
            this.txtfixedBy.TabIndex = 109;
            // 
            // txtreportedBy
            // 
            this.txtreportedBy.Location = new System.Drawing.Point(773, 15);
            this.txtreportedBy.Name = "txtreportedBy";
            this.txtreportedBy.ReadOnly = true;
            this.txtreportedBy.Size = new System.Drawing.Size(169, 20);
            this.txtreportedBy.TabIndex = 108;
            // 
            // txtfixedDate
            // 
            this.txtfixedDate.Location = new System.Drawing.Point(375, 49);
            this.txtfixedDate.Name = "txtfixedDate";
            this.txtfixedDate.ReadOnly = true;
            this.txtfixedDate.Size = new System.Drawing.Size(118, 20);
            this.txtfixedDate.TabIndex = 107;
            // 
            // txtreportedDate
            // 
            this.txtreportedDate.Location = new System.Drawing.Point(118, 52);
            this.txtreportedDate.Name = "txtreportedDate";
            this.txtreportedDate.ReadOnly = true;
            this.txtreportedDate.Size = new System.Drawing.Size(118, 20);
            this.txtreportedDate.TabIndex = 106;
            // 
            // txtprojectName
            // 
            this.txtprojectName.Location = new System.Drawing.Point(550, 14);
            this.txtprojectName.Name = "txtprojectName";
            this.txtprojectName.ReadOnly = true;
            this.txtprojectName.Size = new System.Drawing.Size(123, 20);
            this.txtprojectName.TabIndex = 105;
            // 
            // txtbugStatus
            // 
            this.txtbugStatus.Location = new System.Drawing.Point(318, 17);
            this.txtbugStatus.Name = "txtbugStatus";
            this.txtbugStatus.ReadOnly = true;
            this.txtbugStatus.Size = new System.Drawing.Size(123, 20);
            this.txtbugStatus.TabIndex = 104;
            // 
            // txtBug
            // 
            this.txtBug.Location = new System.Drawing.Point(77, 93);
            this.txtBug.Multiline = true;
            this.txtBug.Name = "txtBug";
            this.txtBug.ReadOnly = true;
            this.txtBug.Size = new System.Drawing.Size(341, 192);
            this.txtBug.TabIndex = 103;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(77, 14);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(122, 20);
            this.txtId.TabIndex = 102;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 101;
            this.label2.Text = "Bug:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(469, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 13);
            this.label13.TabIndex = 100;
            this.label13.Text = "Project Name";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(237, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 99;
            this.label12.Text = "Bug Status";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(527, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 98;
            this.label7.Text = "Fixed By";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(690, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 97;
            this.label3.Text = "Reported By";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(292, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 96;
            this.label6.Text = "Fixed Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 95;
            this.label5.Text = "Reported Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 94;
            this.label1.Text = "Bug ID:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(150, 589);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(55, 13);
            this.linkLabel1.TabIndex = 124;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "linkLabel1";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // ViewReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(0, 200);
            this.ClientSize = new System.Drawing.Size(997, 572);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.pbErrorImage);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pbBugSolution);
            this.Controls.Add(this.textBoxBugSolution);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtfixedBy);
            this.Controls.Add(this.txtreportedBy);
            this.Controls.Add(this.txtfixedDate);
            this.Controls.Add(this.txtreportedDate);
            this.Controls.Add(this.txtprojectName);
            this.Controls.Add(this.txtbugStatus);
            this.Controls.Add(this.txtBug);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "ViewReport";
            this.Text = "ViewReport";
            this.Load += new System.EventHandler(this.ViewReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBugSolution)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbErrorImage;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pbBugSolution;
        private System.Windows.Forms.RichTextBox richTextBoxBugDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxBugSolution;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtfixedBy;
        private System.Windows.Forms.TextBox txtreportedBy;
        private System.Windows.Forms.TextBox txtfixedDate;
        private System.Windows.Forms.TextBox txtreportedDate;
        private System.Windows.Forms.TextBox txtprojectName;
        private System.Windows.Forms.TextBox txtbugStatus;
        private System.Windows.Forms.TextBox txtBug;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}