﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;
using BusinessLogicLayer;

namespace BugTrackingApplication
{
    public partial class TesterDashboard : Form
    {
        private string reporter;
        public TesterDashboard()
        {
            InitializeComponent();
        }
        public TesterDashboard(string reporter)
        {
            this.reporter = reporter;
            InitializeComponent();
        }

        private void viewProjectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProjectForm projectForm = new ProjectForm();
            projectForm.Show();
        }

        private void reportBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportBug reportBug = new BugTrackingApplication.ReportBug(reporter);
            reportBug.Show();
        }

        private void viewBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BugList bugList = new BugTrackingApplication.BugList();
            bugList.btnUpdate.Enabled = false;
            bugList.Show();
        }

        private void searchBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int role = 0;
            SearchBug bug = new SearchBug(role);
            bug.Show();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword cp = new BugTrackingApplication.ChangePassword(reporter);
            cp.Show();
        }
    }
}
