﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;
using BusinessLogicLayer;
using System.Data.SqlClient;

namespace BugTrackingApplication
{
    public partial class SearchBug : Form
    {
        SqlConnection conn = new SqlConnection(ConnectionClass.ConnectionString);

        private int role;
        public SearchBug()
        {
            InitializeComponent();
        }
        public SearchBug(int role)
        {
            this.role = role;
            InitializeComponent();
        }

        /// <summary>
        /// this helps to open the bug details from project name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {

            string projectName = comboBoxSearch.Text;
            
                try
                {
                    SqlDataAdapter sda = new SqlDataAdapter("select count(*) from BugTable where ProjectName ='" + projectName + "';", conn);

                    DataTable dt = new DataTable();
                    sda.Fill(dt);

                    if (dt.Rows[0][0].ToString() == "0")
                    {
                        MessageBox.Show("NO Bug");
                    }
                    else
                    {
                    
                    BugList list = new BugList(role, projectName);
                    list.Show();
                }
                    conn.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Query Error" + ex.Message);
                }

            }

        private void SearchBug_Load(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter("select ProjectName from ProjectTable", conn);

                DataTable dt = new DataTable();
                sda.Fill(dt);
                comboBoxSearch.DisplayMember = "projectName";
                comboBoxSearch.DataSource = dt;
                conn.Close();



            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBoxSearch_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
