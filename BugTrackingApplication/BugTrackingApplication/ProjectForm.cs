﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;
using BusinessLogicLayer;

namespace BugTrackingApplication
{
    public partial class ProjectForm : Form
    {
        public ProjectForm()
        {
            InitializeComponent();
        }

        protected override bool ProcessDialogKey(Keys keyData) //Closes the form when Escape key is pressed
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        ProjectClass pc = new ProjectClass();
        BusinessLogicClass blc = new BusinessLogicClass();
        private int id;

        private void ProjectForm_Load(object sender, EventArgs e)
        {
            dgvProjectDetails.DataSource = pc.GetAllProject();      
        }

        /// <summary>
        /// insert all the details into the database using form insert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvProjectDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            id = Convert.ToInt32(dgvProjectDetails.SelectedRows[0].Cells["ProjectId"].Value.ToString());
            textBoxProjectName.Text = dgvProjectDetails.SelectedRows[0].Cells["ProjectName"].Value.ToString();
            textBoxProjectDescription.Text = dgvProjectDetails.SelectedRows[0].Cells["ProjectDescription"].Value.ToString();
            dtpStartDate.Text = dgvProjectDetails.SelectedRows[0].Cells["StartDate"].Value.ToString();
            dtpEndDate.Text = dgvProjectDetails.SelectedRows[0].Cells["EndDate"].Value.ToString();
        }

        public void makeFieldsBlank()
        {
            textBoxProjectName.Clear();
            textBoxProjectDescription.Clear();
        }

        public bool DuplicateUser() //validation for valid project name
        {
            int x = 0;
            for (int i = 0; i < dgvProjectDetails.Rows.Count; i++)
            {
                if (textBoxProjectName.Text == dgvProjectDetails.Rows[i].Cells["ProjectName"].Value.ToString())
                { x = 1; }
            }
            if (x == 1)
                return true;
            else
                return false;
        }

        /// <summary>
        /// function to add project details
        /// </summary>
        private void AddProject()
        {
            try
            {
                bool result = blc.ManageProject(0, textBoxProjectName.Text, textBoxProjectDescription.Text, dtpStartDate.Text, dtpEndDate.Text, 1);
                if (result == true)
                {
                    MessageBox.Show("Project Successfully Created");
                    dgvProjectDetails.DataSource = pc.GetAllProject();
                    makeFieldsBlank();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// function to update project details
        /// </summary>
        private void UpdateProject()
        {
            try
            {
                bool result = blc.ManageProject(id, textBoxProjectName.Text, textBoxProjectDescription.Text, dtpStartDate.Text, dtpEndDate.Text, 2);
                if (result == true)
                {
                    MessageBox.Show("Project Successfully Updated");
                    dgvProjectDetails.DataSource = pc.GetAllProject();
                    makeFieldsBlank();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonAddProject_Click(object sender, EventArgs e)
        {
            if (textBoxProjectName.Text == "")
            {
                MessageBox.Show("Please provide Project Name!!");
            }
            else if (textBoxProjectDescription.Text == "")
            { MessageBox.Show("Please provide Project desription!!"); }
            else
            {
                if (DuplicateUser() == true)
                {
                    MessageBox.Show("Project name you entered already exists");
                }
                else
                {
                    AddProject();
                }
            }
        }

        private void btnUpdateProject_Click(object sender, EventArgs e)
        {
            if (textBoxProjectName.Text == "")
            {
                MessageBox.Show("Please provide Project Name!!");
            }
            else if (textBoxProjectDescription.Text == "")
            { MessageBox.Show("Please provide Project desription!!"); }
            else
            {
               
                    UpdateProject();
                
            }
        }

        private void btnDeleteProject_Click(object sender, EventArgs e)
        {
            try
            {
                bool result = blc.ManageProject(id, textBoxProjectName.Text, textBoxProjectDescription.Text, dtpStartDate.Text, dtpEndDate.Text, 3);
                if (result == true)
                {
                    MessageBox.Show("Project Successfully Deleted");
                    dgvProjectDetails.DataSource = pc.GetAllProject();
                    makeFieldsBlank();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
