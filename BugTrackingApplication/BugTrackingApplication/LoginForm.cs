﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;

namespace BugTrackingApplication
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        MemberClass mc = new MemberClass();

        /// <summary>
        /// can be logged in using role of member
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (mc.Login(txtUserName.Text, txtPassword.Text) == true)
                {
                    AdminDashBoard adminDashBoard = new AdminDashBoard();
                    adminDashBoard.Show();
                }
                else
                {
                    MessageBox.Show("Invalid UserName or Password");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            { txtPassword.UseSystemPasswordChar = false; }
            else
            { txtPassword.UseSystemPasswordChar = true; }

        }
        //Closes the form when Escape key is pressed
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            EntertoLogin();
        }

        private void EntertoLogin()
        {
            String Role = mc.RoleBasedLogin(txtUserName.Text, txtPassword.Text);
            if (Role == "")
            { MessageBox.Show("Invalid User Name or Password"); }
            else
            {
                if (Role == "Admin")
                {
                    MessageBox.Show("You are logged in as Admin");
                    AdminDashBoard frm = new AdminDashBoard(txtUserName.Text);
                    BugList bl = new BugList();
                    bl.btnUpdate.Enabled = false;
                    frm.Show();
                    
                }
                else if(Role == "Developer")
                {
                    MessageBox.Show("You are logged in as Developer");
                    DeveloperDashboard dd = new DeveloperDashboard(txtUserName.Text);
                    dd.Show();

                }
                else
                {
                    MessageBox.Show("You are logged in as Tester");
                    TesterDashboard frm = new TesterDashboard(txtUserName.Text);
                    BugList bl = new BugList();
                    bl.btnUpdate.Enabled = false;
                    frm.Show();
                }
            }
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            AdminDashBoard frm = new AdminDashBoard();
            frm.Show();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }
    }
}
