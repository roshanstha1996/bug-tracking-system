﻿namespace BugTrackingApplication
{
    partial class UpdateBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxLink = new System.Windows.Forms.TextBox();
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.textBoxBugSolution = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.richTextBoxBugDescription = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtfixedBy = new System.Windows.Forms.TextBox();
            this.txtreportedBy = new System.Windows.Forms.TextBox();
            this.txtfixedDate = new System.Windows.Forms.TextBox();
            this.txtreportedDate = new System.Windows.Forms.TextBox();
            this.txtprojectName = new System.Windows.Forms.TextBox();
            this.txtbugStatus = new System.Windows.Forms.TextBox();
            this.txtBug = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pbBugSolution = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label9 = new System.Windows.Forms.Label();
            this.pbErrorImage = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBugSolution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorImage)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxLink
            // 
            this.textBoxLink.Location = new System.Drawing.Point(151, 592);
            this.textBoxLink.Name = "textBoxLink";
            this.textBoxLink.Size = new System.Drawing.Size(545, 20);
            this.textBoxLink.TabIndex = 89;
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Location = new System.Drawing.Point(151, 992);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(121, 21);
            this.comboBoxStatus.TabIndex = 88;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 992);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 87;
            this.label14.Text = " New Status";
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(521, 809);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowse.TabIndex = 85;
            this.buttonBrowse.Text = "Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(327, 1037);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(89, 23);
            this.buttonCancel.TabIndex = 84;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(151, 1037);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(89, 23);
            this.buttonUpdate.TabIndex = 83;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // textBoxBugSolution
            // 
            this.textBoxBugSolution.Location = new System.Drawing.Point(151, 627);
            this.textBoxBugSolution.Multiline = true;
            this.textBoxBugSolution.Name = "textBoxBugSolution";
            this.textBoxBugSolution.Size = new System.Drawing.Size(683, 163);
            this.textBoxBugSolution.TabIndex = 82;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 592);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 13);
            this.label10.TabIndex = 80;
            this.label10.Text = "Version Control Link";
            // 
            // richTextBoxBugDescription
            // 
            this.richTextBoxBugDescription.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBoxBugDescription.Location = new System.Drawing.Point(140, 12);
            this.richTextBoxBugDescription.Name = "richTextBoxBugDescription";
            this.richTextBoxBugDescription.ReadOnly = true;
            this.richTextBoxBugDescription.Size = new System.Drawing.Size(817, 273);
            this.richTextBoxBugDescription.TabIndex = 10;
            this.richTextBoxBugDescription.Text = "";
            this.richTextBoxBugDescription.TextChanged += new System.EventHandler(this.richTextBoxBugDescription_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Bug Description";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 635);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 81;
            this.label11.Text = "Bug Solution";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.richTextBoxBugDescription);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(9, 294);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(959, 288);
            this.panel1.TabIndex = 79;
            // 
            // txtfixedBy
            // 
            this.txtfixedBy.Location = new System.Drawing.Point(608, 52);
            this.txtfixedBy.Name = "txtfixedBy";
            this.txtfixedBy.ReadOnly = true;
            this.txtfixedBy.Size = new System.Drawing.Size(169, 20);
            this.txtfixedBy.TabIndex = 78;
            // 
            // txtreportedBy
            // 
            this.txtreportedBy.Location = new System.Drawing.Point(771, 18);
            this.txtreportedBy.Name = "txtreportedBy";
            this.txtreportedBy.ReadOnly = true;
            this.txtreportedBy.Size = new System.Drawing.Size(169, 20);
            this.txtreportedBy.TabIndex = 77;
            // 
            // txtfixedDate
            // 
            this.txtfixedDate.Location = new System.Drawing.Point(373, 52);
            this.txtfixedDate.Name = "txtfixedDate";
            this.txtfixedDate.ReadOnly = true;
            this.txtfixedDate.Size = new System.Drawing.Size(118, 20);
            this.txtfixedDate.TabIndex = 76;
            // 
            // txtreportedDate
            // 
            this.txtreportedDate.Location = new System.Drawing.Point(116, 55);
            this.txtreportedDate.Name = "txtreportedDate";
            this.txtreportedDate.ReadOnly = true;
            this.txtreportedDate.Size = new System.Drawing.Size(118, 20);
            this.txtreportedDate.TabIndex = 75;
            // 
            // txtprojectName
            // 
            this.txtprojectName.Location = new System.Drawing.Point(548, 17);
            this.txtprojectName.Name = "txtprojectName";
            this.txtprojectName.ReadOnly = true;
            this.txtprojectName.Size = new System.Drawing.Size(123, 20);
            this.txtprojectName.TabIndex = 74;
            // 
            // txtbugStatus
            // 
            this.txtbugStatus.Location = new System.Drawing.Point(316, 20);
            this.txtbugStatus.Name = "txtbugStatus";
            this.txtbugStatus.ReadOnly = true;
            this.txtbugStatus.Size = new System.Drawing.Size(123, 20);
            this.txtbugStatus.TabIndex = 73;
            // 
            // txtBug
            // 
            this.txtBug.Location = new System.Drawing.Point(75, 96);
            this.txtBug.Multiline = true;
            this.txtBug.Name = "txtBug";
            this.txtBug.ReadOnly = true;
            this.txtBug.Size = new System.Drawing.Size(341, 192);
            this.txtBug.TabIndex = 72;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(75, 17);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(122, 20);
            this.txtId.TabIndex = 71;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 70;
            this.label2.Text = "Bug:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(467, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 13);
            this.label13.TabIndex = 69;
            this.label13.Text = "Project Name";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(235, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 68;
            this.label12.Text = "Bug Status";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(525, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 67;
            this.label7.Text = "Fixed By";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(688, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 66;
            this.label3.Text = "Reported By";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(290, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 65;
            this.label6.Text = "Fixed Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 64;
            this.label5.Text = "Reported Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "Bug ID:";
            // 
            // pbBugSolution
            // 
            this.pbBugSolution.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbBugSolution.Location = new System.Drawing.Point(151, 809);
            this.pbBugSolution.Name = "pbBugSolution";
            this.pbBugSolution.Size = new System.Drawing.Size(364, 167);
            this.pbBugSolution.TabIndex = 90;
            this.pbBugSolution.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 809);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 91;
            this.label8.Text = "Bug Solution";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(432, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 92;
            this.label9.Text = "Error Image";
            // 
            // pbErrorImage
            // 
            this.pbErrorImage.Location = new System.Drawing.Point(499, 96);
            this.pbErrorImage.Name = "pbErrorImage";
            this.pbErrorImage.Size = new System.Drawing.Size(469, 192);
            this.pbErrorImage.TabIndex = 93;
            this.pbErrorImage.TabStop = false;
            // 
            // UpdateBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(0, 200);
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(997, 749);
            this.Controls.Add(this.pbErrorImage);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pbBugSolution);
            this.Controls.Add(this.textBoxLink);
            this.Controls.Add(this.comboBoxStatus);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.textBoxBugSolution);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtfixedBy);
            this.Controls.Add(this.txtreportedBy);
            this.Controls.Add(this.txtfixedDate);
            this.Controls.Add(this.txtreportedDate);
            this.Controls.Add(this.txtprojectName);
            this.Controls.Add(this.txtbugStatus);
            this.Controls.Add(this.txtBug);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "UpdateBug";
            this.Text = "UpdateBug";
            this.Load += new System.EventHandler(this.UpdateBug_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBugSolution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbErrorImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxLink;
        private System.Windows.Forms.ComboBox comboBoxStatus;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.TextBox textBoxBugSolution;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox richTextBoxBugDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtfixedBy;
        private System.Windows.Forms.TextBox txtreportedBy;
        private System.Windows.Forms.TextBox txtfixedDate;
        private System.Windows.Forms.TextBox txtreportedDate;
        private System.Windows.Forms.TextBox txtprojectName;
        private System.Windows.Forms.TextBox txtbugStatus;
        private System.Windows.Forms.TextBox txtBug;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbBugSolution;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pbErrorImage;
    }
}