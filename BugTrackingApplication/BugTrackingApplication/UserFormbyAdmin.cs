﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;

namespace BugTrackingApplication
{
    public partial class UserFormbyAdmin : Form
    {
        public UserFormbyAdmin()
        {
            InitializeComponent();
        }

        protected override bool ProcessDialogKey(Keys keyData) //Closes the form when Escape key is pressed
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        MemberClass mc = new MemberClass();
        private int id;

        private void UserFormbyAdmin_Load(object sender, EventArgs e)
        {
            dgvUserDetails.DataSource = mc.GetAllUsers();
        }

        private void dgvUserDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            id = Convert.ToInt32(dgvUserDetails.SelectedRows[0].Cells["MemberId"].Value.ToString());
            txtName.Text = dgvUserDetails.SelectedRows[0].Cells["MemberName"].Value.ToString();
            txtAddress.Text = dgvUserDetails.SelectedRows[0].Cells["Address"].Value.ToString();
            txtEmail.Text = dgvUserDetails.SelectedRows[0].Cells["Email"].Value.ToString();
            txtContact.Text = dgvUserDetails.SelectedRows[0].Cells["Contact"].Value.ToString();
            txtGender.Text = dgvUserDetails.SelectedRows[0].Cells["Gender"].Value.ToString();
            txtUserName.Text = dgvUserDetails.SelectedRows[0].Cells["UserName"].Value.ToString();
            cmbRole.Text = dgvUserDetails.SelectedRows[0].Cells["Role"].Value.ToString();
            txtPassword.Text = dgvUserDetails.SelectedRows[0].Cells["Password"].Value.ToString();
            txtConfirmPassword.Text = dgvUserDetails.SelectedRows[0].Cells["Password"].Value.ToString();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text == "")
            {
                MessageBox.Show("Please provide User Name");
            }
            else if (txtName.Text == "")
            { MessageBox.Show("Please provide First Name"); }
            else if (txtAddress.Text == "")
            { MessageBox.Show("Please provide Address"); }
            else if (txtContact.Text == "")
            { MessageBox.Show("Please provide your contact"); }
            else if (txtEmail.Text == "")
            { MessageBox.Show("Please provide Email"); }
            else if (txtGender.Text == "")
            { MessageBox.Show("Please provide Gender"); }
            else if (txtUserName.Text == "")
            { MessageBox.Show("Please provide username"); }
            else if (txtPassword.Text == "")
            { MessageBox.Show("Please provide password"); }
            else if (txtPassword.Text != txtConfirmPassword.Text)
            {
                MessageBox.Show("Password you entered did not match");
                txtPassword.Clear();
                txtConfirmPassword.Clear();
            }
            else
            {
                if (DuplicateUser() == true)
                {
                    MessageBox.Show("Username you entered already exists");
                }
                else
                {
                    AddUser();
                }
            }
        }

        private void AddUser()
        {
            try
            {
                
                int result = mc.ManageMember(0, txtName.Text, txtAddress.Text, txtContact.Text, txtEmail.Text, txtGender.Text, txtUserName.Text,txtPassword.Text,cmbRole.Text, 1);
                if (result > 0)
                {
                    MessageBox.Show("User Successfully Created");
                    dgvUserDetails.DataSource = mc.GetAllUsers();
                    makeFieldsBlank();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void makeFieldsBlank()
        {
            txtName.Clear();
            txtAddress.Clear();
            txtContact.Clear();
            txtEmail.Clear();
            txtGender.Clear();
            txtUserName.Clear();
            txtPassword.Clear();
            txtConfirmPassword.Clear();
            cmbRole.Text = "";
        }

        public bool DuplicateUser()
        {
            int x = 0;
            for (int i = 0; i < dgvUserDetails.Rows.Count; i++)
            {
                if (txtUserName.Text == dgvUserDetails.Rows[i].Cells["UserName"].Value.ToString())
                { x = 1; }
            }
            if (x == 1)
                return true;
            else
                return false;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text == "")
            {
                MessageBox.Show("Please provide User Name");
            }
            else if (txtName.Text == "")
            { MessageBox.Show("Please provide First Name"); }
            else if (txtAddress.Text == "")
            { MessageBox.Show("Please provide Address"); }
            else if (txtContact.Text == "")
            { MessageBox.Show("Please provide your contact"); }
            else if (txtEmail.Text == "")
            { MessageBox.Show("Please provide Email"); }
            else if (txtGender.Text == "")
            { MessageBox.Show("Please provide Gender"); }
            else if (txtUserName.Text == "")
            { MessageBox.Show("Please provide username"); }
            else if (txtPassword.Text == "")
            { MessageBox.Show("Please provide password"); }
            else if (txtPassword.Text != txtConfirmPassword.Text)
            {
                MessageBox.Show("Password you entered did not match");
                txtPassword.Clear();
                txtConfirmPassword.Clear();
            }
            else
            {
                UpdateUser();
            }
        }

        private void UpdateUser()
        {
            try
            {
                int result = mc.ManageMember(id, txtName.Text, txtAddress.Text, txtContact.Text, txtEmail.Text, txtGender.Text, txtUserName.Text,txtPassword.Text,cmbRole.Text, 2);
                if (result > 0)
                {
                    MessageBox.Show("User Successfully Updated");
                    dgvUserDetails.DataSource = mc.GetAllUsers();
                    makeFieldsBlank();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
             try
            {
                int result = mc.ManageMember(id, txtName.Text, txtAddress.Text, txtContact.Text, txtEmail.Text, txtGender.Text, txtUserName.Text, txtPassword.Text, cmbRole.Text, 3);
                if (result > 0)
                {
                    MessageBox.Show("User Successfully Deleted");
                    dgvUserDetails.DataSource = mc.GetAllUsers();
                    makeFieldsBlank();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvUserDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
