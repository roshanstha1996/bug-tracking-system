﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;
using BusinessLogicLayer;

namespace BugTrackingApplication
{
    public partial class AdminDashBoard : Form
    {
        string reportedBy;
        public AdminDashBoard()
        {
            InitializeComponent();
        }

        /// <summary>
        /// made a constructer of the form by providing a parameters
        /// </summary>
        /// <param name="reportedBy"></param>
        public AdminDashBoard(string reportedBy) 
        {
            this.reportedBy = reportedBy;
            InitializeComponent();
        }

        private void manageUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserFormbyAdmin frm = new UserFormbyAdmin();
            frm.MdiParent = this; 
            frm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void createProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProjectForm frm1 = new ProjectForm();
            frm1.MdiParent = this;
            frm1.Show();
        }

        private void reportBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportBug rp = new ReportBug(reportedBy);
            rp.Show();
        }

        private void viewBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BugList bugList = new BugTrackingApplication.BugList();
            bugList.btnUpdate.Enabled = false;
            bugList.Show();
        }
    }
}
