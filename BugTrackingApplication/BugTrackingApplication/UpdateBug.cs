﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;
using BusinessLogicLayer;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.IO;

namespace BugTrackingApplication
{
    public partial class UpdateBug : Form
    {
        SqlConnection conn = new SqlConnection(ConnectionClass.ConnectionString);

        private int bugId;
        private string bugFixedBy; //Bug fixer username
        private string filepath; // image path

        BugClass bugClass = new BugClass();
        BusinessLogicClass blc = new BusinessLogicClass();

        public UpdateBug()
        {
            InitializeComponent();
            loadBug();
        }


        public UpdateBug(int bugId, string bugFixedBy)
        {
            this.bugId = bugId;
            this.bugFixedBy = bugFixedBy;
            InitializeComponent();
            loadBug();
            

        }

        private void ManageBugs()
        {
            try
            {
                bool result = blc.ManageBugs(bugId,DateTime.Now,txtfixedBy.Text ,filepath,textBoxBugSolution.Text, textBoxLink.Text,comboBoxStatus.Text, 2);
                if (result == true)
                {
                    MessageBox.Show("Bug report added!!");
                }
                else
                {
                    MessageBox.Show("Unable to report BUg!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void loadBug()
        {

            try
            {
                conn.Open();
                String selectQuery = "SELECT * FROM BugTable WHERE BugId = '" + bugId.ToString() + "'";

                SqlCommand command = new SqlCommand(selectQuery, conn);

                SqlDataAdapter da = new SqlDataAdapter(command);

                DataTable table = new DataTable();

                da.Fill(table);
                txtId.Text = table.Rows[0][0].ToString();
                txtprojectName.Text = table.Rows[0][1].ToString();
                txtreportedDate.Text = table.Rows[0][2].ToString();
                txtfixedDate.Text = table.Rows[0][3].ToString();
                txtBug.Text = table.Rows[0][4].ToString();
                richTextBoxBugDescription.Text=table.Rows[0][5].ToString();
                txtreportedBy.Text = table.Rows[0][6].ToString();
                txtfixedBy.Text = table.Rows[0][7].ToString();
                byte[] errorImage = (byte[])table.Rows[0][8];

                //byte[] solvedImmages = (byte[])table.Rows[0][9];
                textBoxBugSolution.Text= table.Rows[0][10].ToString();
                textBoxLink.Text = table.Rows[0][11].ToString();
                comboBoxStatus.Text = table.Rows[0][12].ToString();
                txtbugStatus.Text = table.Rows[0][12].ToString();

                //MemoryStream ms_bugsymptoms = new MemoryStream(bugsymptoms);
                MemoryStream ms_errorImage = new MemoryStream(errorImage);
                pbErrorImage.Image = Image.FromStream(ms_errorImage);
               // pictureBox2.Image = Image.FromStream(ms_bugsymptoms);



                da.Dispose();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error" + ex.Message);
            }
        }
        private void UpdateBug_Load(object sender, EventArgs e)
        {
            comboBoxStatus.Items.Add("Not fixed");
            comboBoxStatus.Items.Add("Fixed");
            //get syntax color on form viewBug_load


            // getting keywords/functions
            string keywords = @"\b(public|private|partial|static|namespace|class|using|void|foreach|in)\b";
            MatchCollection keywordMatches = Regex.Matches(richTextBoxBugDescription.Text, keywords);

            // getting types/classes from the text 
            string types = @"\b(Console)\b";
            MatchCollection typeMatches = Regex.Matches(richTextBoxBugDescription.Text, types);

            // getting comments (inline or multiline)
            string comments = @"(\/\/.+?$|\/\*.+?\*\/)";
            MatchCollection commentMatches = Regex.Matches(richTextBoxBugDescription.Text, comments, RegexOptions.Multiline);

            // getting strings
            string strings = "\".+?\"";
            MatchCollection stringMatches = Regex.Matches(richTextBoxBugDescription.Text, strings);

            // saving the original caret position + forecolor
            int originalIndex = richTextBoxBugDescription.SelectionStart;
            int originalLength = richTextBoxBugDescription.SelectionLength;
            Color originalColor = Color.Black;

            // MANDATORY - focuses a label before highlighting (avoids blinking)
            label4.Focus();

            // removes any previous highlighting (so modified words won't remain highlighted)
            richTextBoxBugDescription.SelectionStart = 0;
            richTextBoxBugDescription.SelectionLength = richTextBoxBugDescription.Text.Length;
            richTextBoxBugDescription.SelectionColor = originalColor;

            // scanning...
            foreach (Match m in keywordMatches)
            {
                richTextBoxBugDescription.SelectionStart = m.Index;
                richTextBoxBugDescription.SelectionLength = m.Length;
                richTextBoxBugDescription.SelectionColor = Color.Blue;
            }

            foreach (Match m in typeMatches)
            {
                richTextBoxBugDescription.SelectionStart = m.Index;
                richTextBoxBugDescription.SelectionLength = m.Length;
                richTextBoxBugDescription.SelectionColor = Color.DarkCyan;
            }

            foreach (Match m in commentMatches)
            {
                richTextBoxBugDescription.SelectionStart = m.Index;
                richTextBoxBugDescription.SelectionLength = m.Length;
                richTextBoxBugDescription.SelectionColor = Color.Green;
            }

            foreach (Match m in stringMatches)
            {
                richTextBoxBugDescription.SelectionStart = m.Index;
                richTextBoxBugDescription.SelectionLength = m.Length;
                richTextBoxBugDescription.SelectionColor = Color.Brown;
            }

            // restoring the original colors, for further writing
            richTextBoxBugDescription.SelectionStart = originalIndex;
            richTextBoxBugDescription.SelectionLength = originalLength;
            richTextBoxBugDescription.SelectionColor = originalColor;

            // giving back the focus
            richTextBoxBugDescription.Focus();
        }

        private void txtBug_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtprojectName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtbugStatus_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtfixedDate_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtreportedDate_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtfixedBy_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtreportedBy_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBoxBugDescription_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filepath = openFileDialog1.FileName;
                pbBugSolution.Image = Image.FromFile(filepath);
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            ManageBugs();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
