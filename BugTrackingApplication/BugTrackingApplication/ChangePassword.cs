﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;
using BusinessLogicLayer;

namespace BugTrackingApplication
{
    public partial class ChangePassword : Form
    {
        private string userName;

        MemberClass mc = new MemberClass();

        public ChangePassword()
        {
            InitializeComponent();
        }

        public ChangePassword(string userName)
        {
            this.userName = userName;
            InitializeComponent();
            txtUserName.Text = this.userName;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {




            string username = txtUserName.Text;
            string currentPassword = txtCurrentPassword.Text; ;
            string newPassword = txtNewPassword.Text;
            string confirmPassword = txtConfirmPassword.Text;
            if (!string.IsNullOrEmpty(currentPassword) && !string.IsNullOrEmpty(newPassword) && !string.IsNullOrEmpty(confirmPassword))
            {
                if (newPassword == confirmPassword)
                {
                    mc.UpdatePassword(userName, newPassword);


                    MessageBox.Show("Password Updated!");
                    txtCurrentPassword.Text = "";
                    txtNewPassword.Text = "";
                    txtConfirmPassword.Text = "";
                    txtCurrentPassword.Focus();



                }
                else
                {
                    MessageBox.Show("The current password you entered is incorrect!");
                    txtCurrentPassword.Focus();
                }
            }
            else
            {
                MessageBox.Show("The new password and confirm password fields does not match!");
                txtConfirmPassword.Focus();
            }
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

