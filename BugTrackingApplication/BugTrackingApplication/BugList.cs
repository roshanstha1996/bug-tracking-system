﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;
using BusinessLogicLayer;

namespace BugTrackingApplication
{
    public partial class BugList : Form
    {
        private string fixedby;
        private string projectName;
        private int id;

        public BugList()
        {
            InitializeComponent();
            loadAllBug();
        }
        public BugList(int id ,string projectName)
        {
            this.id = id;
            this.projectName = projectName;
            InitializeComponent();
            loadAllBug();
            if(this.id.ToString() == "0")
            {
                btnUpdate.Enabled = false;
            }
        }
        public BugList( string fixedby)
        {
            this.fixedby = fixedby;
            InitializeComponent();
            loadAllBug();
        }
        BugClass bc = new BugClass();
        MemberClass mc = new MemberClass();
        BusinessLogicClass blc = new BusinessLogicClass();



        private void loadAllBug()
        {
            if (projectName != null)
            {
                dataGridViewBugList.DataSource = bc.GetBugbyProjectName(projectName);
            }
            else
            {
                dataGridViewBugList.DataSource = bc.GetBugs();
            }
           
            dataGridViewBugList.Rows.Remove(dataGridViewBugList.Rows[0]);
            dataGridViewBugList.Columns["BugId"].HeaderText = "BugId";
            dataGridViewBugList.Columns["fixedStatus"].HeaderText = "fixedStatus";
            dataGridViewBugList.Columns["ProjectName"].HeaderText = "ProjectName";
            dataGridViewBugList.Columns["EntryDate"].HeaderText = "EntryDate";
            dataGridViewBugList.Columns["BugDetails"].HeaderText = "BugDetails";
            dataGridViewBugList.Columns["BugCode"].HeaderText = "BugCode";
            dataGridViewBugList.Columns["Reportedby"].HeaderText = "Reportedby";
            dataGridViewBugList.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridViewBugList.AllowUserToAddRows = false;
            dataGridViewBugList.AllowUserToDeleteRows = false;
            dataGridViewBugList.MultiSelect = false;
            dataGridViewBugList.AllowUserToResizeRows = false;
            dataGridViewBugList.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        }
        
        /// <summary>
        /// to show the fixed or not fixed error of bug list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BugList_Load(object sender, EventArgs e)
        {

            
            foreach (DataGridViewRow row in dataGridViewBugList.Rows)
            {

                string status = row.Cells[1].Value.ToString(); 

                if (status == "Fixed")
                {
                    row.DefaultCellStyle.BackColor = Color.GreenYellow;
                    row.DefaultCellStyle.ForeColor = Color.Black;
                }
                else
                {
                    row.DefaultCellStyle.BackColor = Color.OrangeRed;
                    row.DefaultCellStyle.ForeColor = Color.White;
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            if (dataGridViewBugList.SelectedRows.Count != 0)
            {
                int bugId = Convert.ToInt32(dataGridViewBugList.SelectedRows[0].Cells[0].Value);

                UpdateBug bug = new UpdateBug(bugId,fixedby);
                bug.ShowDialog();

            }
            else
            {
                MessageBox.Show("No row selected! Please select a row!");
            }
            
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            dataGridViewBugList.Refresh();
            if (projectName != null)
            {
                dataGridViewBugList.DataSource = bc.GetBugbyProjectName(projectName);
            }
            else
            {
                dataGridViewBugList.DataSource = bc.GetBugs();
            }

            foreach (DataGridViewRow row in dataGridViewBugList.Rows)
            {
                
                string status = row.Cells[1].Value.ToString();

                if (status == "Fixed")
                {
                    row.DefaultCellStyle.BackColor = Color.GreenYellow;
                    row.DefaultCellStyle.ForeColor = Color.Black;
                }
                else
                {
                    row.DefaultCellStyle.BackColor = Color.OrangeRed;
                    row.DefaultCellStyle.ForeColor = Color.White;
                }
            }

        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (dataGridViewBugList.SelectedRows.Count != 0)
            {
                int bugId = Convert.ToInt32(dataGridViewBugList.SelectedRows[0].Cells[0].Value);

                ViewReport bug = new ViewReport(bugId);
                bug.ShowDialog();

            }
            else
            {
                MessageBox.Show("No row selected! Please select a row!");
            }
        }
    }
}
