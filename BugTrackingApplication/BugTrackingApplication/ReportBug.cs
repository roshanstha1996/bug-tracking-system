﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;
using BusinessLogicLayer;
using System.IO;

namespace BugTrackingApplication
{
    public partial class ReportBug : Form
    {
        private string reporter;


        /// <summary>
        /// helps to report bug in  update bug and validation also done
        /// </summary>
        public ReportBug()
        {
            InitializeComponent();
        }

        public ReportBug(string reporter)
        {
            this.reporter = reporter;
            InitializeComponent();
        }




        protected override bool ProcessDialogKey(Keys keyData) //Closes the form when Escape key is pressed
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }
        
        BugClass bc = new BugClass();
        ProjectClass pc = new ProjectClass();
        BusinessLogicClass blc = new BusinessLogicClass();
        string filepath;      

        private void ReportBug_Load(object sender, EventArgs e)
        {
            comboBoxStatus.Items.Add("Not fixed");
            comboBoxStatus.Items.Add("Fixed");
            comboBoxStatus.SelectedItem = "Not fixed";

            try
            {

                comboBoxProjectName.DataSource = pc.GetProjectbyProjectName();
                comboBoxProjectName.DisplayMember = "ProjectName";
                
                comboBoxProjectName.ValueMember = "ProjectName";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
        }

        private void buttonaddProject_Click(object sender, EventArgs e)
        {
            string bugStatus = comboBoxStatus.Text;
            string projectName = comboBoxProjectName.Text;
            string bugTitle = textBoxBug.Text;
            string bugDescription = richTextBoxbugDescription.Text;
            string gitLink = txtvcLink.Text;
            string bugReporter = reporter;

            if (!string.IsNullOrEmpty(bugStatus) && !string.IsNullOrEmpty(bugDescription)
              && !string.IsNullOrEmpty(gitLink) 
             && !string.IsNullOrEmpty(projectName) && !string.IsNullOrEmpty(bugTitle))
            {
                AddBugReport();
            }
            else
            {
                MessageBox.Show("Please input all the fields!");
            }
        }


        /// <summary>
        /// browse the needed screenshot for error with help of openfileDialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBrowseImage_Click(object sender, EventArgs e)
        {
            
            OpenFileDialog ofd = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filepath = openFileDialog1.FileName;
                pictureBox1.Image = Image.FromFile(filepath);
            }
        }

        /// <summary>
        /// allows the function to add or report the bug details
        /// </summary>
        private void AddBugReport()
        {
            try
            {
                bool result = blc.BugsEntry(0, comboBoxProjectName.Text,DateTime.Now, textBoxBug.Text, richTextBoxbugDescription.Text,reporter, filepath,txtvcLink.Text,comboBoxStatus.Text, 1);
                if (result == true)
                {
                    MessageBox.Show("Bug report added!!");
                }
                else
                {
                    MessageBox.Show("Unable to report BUg!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
