﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class ProjectClass
    {
        SqlConnection conn = new SqlConnection(ConnectionClass.ConnectionString);

        public int ManageProject(int ProjectId,
            String ProjectName,
            String ProjectDescription,
            String StartDate,
            String EndDate,
            int Mode
            )
        {
            try
            {
                int result = 0;
                String txtSql = "";
                if (Mode == 1)
                    txtSql = "insert into ProjectTable(ProjectName,ProjectDescription,StartDate,EndDate) values (@ProjectName,@ProjectDescription,@StartDate,@EndDate)";
                if (Mode == 2)
                    txtSql = "Update ProjectTable set ProjectName=@ProjectName,ProjectDescription=@ProjectDescription,StartDate=@StartDate,EndDate=@EndDate where ProjectId=@ProjectId";
                if (Mode == 3)
                    txtSql = "Delete from ProjectTable where ProjectId=@ProjectId";
                SqlCommand cmd = new SqlCommand(txtSql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ProjectId", ProjectId);
                cmd.Parameters.AddWithValue("@ProjectName", ProjectName);
                cmd.Parameters.AddWithValue("@ProjectDescription", ProjectDescription);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                conn.Open();
                result = cmd.ExecuteNonQuery();
                conn.Close();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { conn.Close(); }
        }

        public DataTable GetAllProject()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select * from ProjectTable", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable GetProjectbyProjectName()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select ProjectName from ProjectTable", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
