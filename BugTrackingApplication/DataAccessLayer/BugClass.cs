﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace DataAccessLayer
{
    public class BugClass
    {
        SqlConnection conn = new SqlConnection(ConnectionClass.ConnectionString);
        public int BugsEntry(int BugId,
            string ProjectName,
            DateTime EntryDate,
            string BugDetails,
            string BugCode,
            string ReportedBy,
            string ErrorImage,
            string versioncontrolLink,
            string fixedStatus,
            int Mode)
        {
            try
            {

                FileStream fs;
                SqlParameter picpara;
                Byte[] bindata;
                int res = 0;
                SqlCommand cmd = new SqlCommand("SP_BugEntry", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BugId", BugId);
                cmd.Parameters.AddWithValue("@ProjectName", ProjectName);
                cmd.Parameters.AddWithValue("@EntryDate", EntryDate);
                cmd.Parameters.AddWithValue("@BugDetails", BugDetails);
                cmd.Parameters.AddWithValue("@BugCode", BugCode);
                cmd.Parameters.AddWithValue("@ReportedBy", ReportedBy);
                picpara = cmd.Parameters.Add("@ErrorImage", SqlDbType.Image);
                cmd.Parameters.AddWithValue("@versioncontrolLink", versioncontrolLink);
                cmd.Parameters.AddWithValue("@fixedStatus", fixedStatus);
                cmd.Parameters.AddWithValue("@Mode", Mode);
                conn.Open();


                cmd.Prepare();
                fs = new FileStream(ErrorImage, FileMode.Open, FileAccess.Read);
                bindata = new byte[Convert.ToInt32(fs.Length)];
              
                fs.Read(bindata, 0, Convert.ToInt32(fs.Length));
                fs.Close();
                


                picpara.Value = bindata;
                
                res = cmd.ExecuteNonQuery();
                conn.Close();
                return res;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { conn.Close(); }
        }

        public int ManageBugs(int BugId,
            DateTime FixedDate,
            string FixedBy,
            string SolvedImage,
            string SolvedDescription,
            string versioncontrolLink,
            string fixedStatus,
            int Mode)
        {
            try
            {

                FileStream fs1;
                SqlParameter picpara1;
                Byte[] bindata1;
                int res = 0;
                SqlCommand cmd = new SqlCommand("SP_ManageBugs", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BugId", BugId);
                cmd.Parameters.AddWithValue("@FixedDate", FixedDate);
                cmd.Parameters.AddWithValue("@FixedBy", FixedBy);
                picpara1 = cmd.Parameters.Add("@SolvedImage", SqlDbType.Image);
                cmd.Parameters.AddWithValue("@SolvedDescription", SolvedDescription);
                cmd.Parameters.AddWithValue("@versioncontrolLink", versioncontrolLink);
                cmd.Parameters.AddWithValue("@fixedStatus", fixedStatus);
                cmd.Parameters.AddWithValue("@Mode", Mode);
                conn.Open();


                cmd.Prepare();
                fs1= new FileStream(SolvedImage, FileMode.Open, FileAccess.Read);
                bindata1 = new byte[Convert.ToInt32(fs1.Length)];
                fs1.Read(bindata1, 0, Convert.ToInt32(fs1.Length));
                fs1.Close();
                picpara1.Value = bindata1;

                res = cmd.ExecuteNonQuery();
                conn.Close();
                return res;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { conn.Close(); }
        }

        public DataTable GetAllBugs()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select * from BugTable", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { conn.Close(); }
        }

        public DataTable GetBugs()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select BugId,fixedStatus,ProjectName,EntryDate,BugDetails,BugCode,Reportedby from BugTable", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { conn.Close(); }
        }

        public DataTable GetBugbyProjectName(string projectName)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select BugId,fixedStatus,ProjectName,EntryDate,BugDetails,BugCode,Reportedby from BugTable where ProjectName='" + projectName + "';", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { conn.Close(); }
        }

        public int BugsCount()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select count(*) from BugTable", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                int totalBugs = Convert.ToInt32(dt.Rows[0][0].ToString());
                return totalBugs;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { conn.Close(); }
        }

        public DataTable GetBugsByMemberId(int MemberId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select * from BugTable where MemberId=@MemberId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@MemberId", MemberId);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { conn.Close(); }
        }

        public DataTable GetBugsByMemberAndProject(int MemberId, int ProjectId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select BugId,BugDetails from BugTable where MemberId=@MemberId and ProjectId=@ProjectId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@MemberId", MemberId);
                cmd.Parameters.AddWithValue("@ProjectId", ProjectId);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { conn.Close(); }
        }
    }
}

