﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class MemberClass
    {
        SqlConnection conn = new SqlConnection(ConnectionClass.ConnectionString);

        public int ManageMember(int MemberId,
            String MemberName,
            String Address,
            String Contact,
            String Email,
            String Gender,
            String UserName,
            String Password,
            String Role,
            int Mode
            )
        {
            try
            {
                int result = 0;
                String txtSql = "";
                if (Mode == 1)
                    txtSql = "insert into MemberTable(MemberName,Address,Contact,Email,Gender,UserName,Password,Role) values (@MemberName,@Address,@Contact,@Email,@Gender,@UserName,@Password,@Role)";
                if (Mode == 2)
                    txtSql = "Update MemberTable set MemberName=@MemberName,Address=@Address,Contact=@Contact,Email=@Email,Gender=@Gender,UserName=@UserName,Password=@Password,Role=@Role where MemberId=@MemberId";
                if (Mode == 3)
                    txtSql = "Delete from MemberTable where MemberId=@MemberId";
                SqlCommand cmd = new SqlCommand(txtSql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@MemberId", MemberId);
                cmd.Parameters.AddWithValue("@MemberName", MemberName);
                cmd.Parameters.AddWithValue("@Address", Address);
                cmd.Parameters.AddWithValue("@Contact", Contact);
                cmd.Parameters.AddWithValue("@Email", Email);
                cmd.Parameters.AddWithValue("@Gender", Gender);
                cmd.Parameters.AddWithValue("@UserName", UserName);
                cmd.Parameters.AddWithValue("@Password", Password);
                cmd.Parameters.AddWithValue("@Role", Role);
                conn.Open();
                result = cmd.ExecuteNonQuery();
                conn.Close();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { conn.Close(); }
        }

        public DataTable GetAllUsers()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select * from MemberTable", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable GetUserRole()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select Role from MemberTable", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Login(String UserName, String Password)
        {
            try
            {
                bool result = false;
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select * from MemberTable where UserName=@UserName and Password=@Password", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UserName", UserName);
                cmd.Parameters.AddWithValue("@Password", Password);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                if (dt.Rows.Count > 0)
                    result = true;
                else
                    result = false;
                conn.Close();
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool UpdatePassword(String userName, string password)
        {
            try
            {
                bool result = false;
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("update MemberTable set Password=@Password where UserName='" + userName + "'", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@Password", password);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                if (dt.Rows.Count > 0)
                    result = true;
                else
                    result = false;
                conn.Close();
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public String RoleBasedLogin(String UserName, String Password)
        {
            try
            {
                String Role = "";
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("Select Role from MemberTable where UserName=@UserName and Password=@Password", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UserName", UserName);
                cmd.Parameters.AddWithValue("@Password", Password);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dt.Load(dr);
                conn.Close();
                if (dt.Rows.Count > 0)
                    Role = dt.Rows[0][0].ToString();
                else
                    Role = "";
                return Role;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
