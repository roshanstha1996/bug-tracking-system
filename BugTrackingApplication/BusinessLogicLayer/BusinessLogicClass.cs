﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public class BusinessLogicClass
    {
        MemberClass mc = new MemberClass();
        ProjectClass pc = new ProjectClass();
        BugClass bc = new BugClass();

        public bool ManageMember(int MemberId,
            String MemberName,
            String Address,
            String Contact,
            String Email,
            String Gender,
            String UserName,
            String Password,
            String Role,
            int Mode
            )
        {
            try
            {
                bool result = false;
                int rs = mc.ManageMember(MemberId,
                    MemberName,
                    Address,
                    Contact,
                    Email,
                    Gender,
                    UserName,
                    Password,
                    Role,
                    Mode);
                if (rs > 0)
                    result = true;
                else
                    result = false;
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ManageProject(int ProjectId,
          String ProjectName,
          String ProjectDescription,
          String StartDate,
          String EndDate,
          int Mode)
        {
            try
            {
                bool result = false;
                int rs = pc.ManageProject(ProjectId, ProjectName, ProjectDescription, StartDate, EndDate, Mode);
                if (rs > 0)
                    result = true;
                else
                    result = false;
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool ManageBugs(int BugId,
            DateTime FixedDate,
            string FixedBy,
            string SolvedImage,
            string SolvedDescription,
            string versioncontrolLink,
            string fixedStatus,
            int Mode)
        {
            try
            {
                bool result = false;
                int rs = bc.ManageBugs(BugId,FixedDate,FixedBy,
                    SolvedImage,SolvedDescription,versioncontrolLink,fixedStatus,Mode);
                if (rs > 0)
                    result = true;
                else
                    result = false;
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool BugsEntry(int BugId,
            string ProjectName,
            DateTime EntryDate,
            string BugDetails,
            string BugCode,
            string ReportedBy,
            string ErrorImage,
            string versioncontrolLink,
            string fixedStatus,
            int Mode)
        {
            try
            {
                int rs = bc.BugsEntry(BugId,
            ProjectName,
            EntryDate,
            BugDetails,
            BugCode,
            ReportedBy,
            ErrorImage,
            versioncontrolLink,
            fixedStatus,
            Mode);
                if (rs > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
